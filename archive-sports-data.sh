#!/bin/bash

# Load functions for archiving sports data
CONFIG_FILE='./archive-config.json'
REMOTE_CONFIG='./remote-config.cfg'
ARCHIVE_UTILS='./archive-utils.sh'

TIME_FLAG_FOLDER="./export-time-flag"
EXPORT_FOLDER="./export-data"
UPLOAD_FILE_EXT='tar.gz'

CHECK_FILES=("${CONFIG_FILE}" "${REMOTE_CONFIG}")

if [ -f "${ARCHIVE_UTILS}" ];then
    . "${ARCHIVE_UTILS}" --source-only
else
    echo "[FindFileError] Can't find ${ARCHIVE_UTILS}"
    exit 1
fi

for file_name in "${CHECK_FILES[@]}"
do
    if [ ! -f "${file_name}" ];then
        error_echo "[FindFileError] Can't find ${file_name}"
        exit 1
    fi
done

# Loading remote DB information.
source "${REMOTE_CONFIG}"

EXPORT_DATA_STATUS=1
# Archive the data
for args in $( load_config "${CONFIG_FILE}" )
do
    # Get parameters from configuration.
    DB_NAME=$( echo "${args}" | jq '.db_name' | sed 's/"//g' )
    COLLECTION_NAME=$( echo "${args}" | jq '.collection_name' | sed 's/"//g' )
    DAY_MINUS=$( echo "${args}" | jq '.day_minus' | sed 's/"//g' )
    TIME_INTERVAL=$( echo "${args}" | jq '.time_interval' | sed 's/"//g' )
    THRESHOLD_DOCS=$( echo "${args}" | jq '.threshold_docs' | sed 's/"//g' )

    log_info=(
        "DB_NAME: ${DB_NAME}, COLLECTION_NAME: ${COLLECTION_NAME}, DAY_MINUS: ${DAY_MINUS}, "
        "TIME_INTERVAL: ${TIME_INTERVAL}, THRESHOLD_DOCS: ${THRESHOLD_DOCS}"
    )
    log_info=$( printf "%-s" "${log_info[@]}" )
    echo "${log_info}"
    echo "------------------------------------------------------------------------------------------------------------------------"
    export_mongo_data ${DB_USER} ${PASSWORD} ${HOST} ${PORT} ${DB_NAME} ${COLLECTION_NAME}\
                     ${DAY_MINUS} ${TIME_INTERVAL} ${THRESHOLD_DOCS} ${TIME_FLAG_FOLDER}\
                     ${EXPORT_FOLDER}
  
    # That the `EXPORT_DATA_STATUS` equal to 0 means its execution without error.
    if [ "${EXPORT_DATA_STATUS}" -eq 0 ] && [ "${?}" -eq 0 ];then
        EXPORT_DATA_DIR="${EXPORT_FOLDER}/${DB_NAME}/${COLLECTION_NAME}"
        echo 'Compress export data.....'
        compress_data "${EXPORT_DATA_DIR}" true

        echo "Upload data to S3....."
        upload_data_to_s3 "${S3_PROFILE_NAME}" "${EXPORT_DATA_DIR}" "${S3_URI_PREFIX}/${DB_NAME}/${COLLECTION_NAME}" "${UPLOAD_FILE_EXT}"

        echo "Check successful upload to S3 and send notification....."
        check_upload_s3 "${S3_PROFILE_NAME}" "${EXPORT_DATA_DIR}" "${S3_URI_PREFIX}/${DB_NAME}/${COLLECTION_NAME}"\
                "${DB_NAME}" "${COLLECTION_NAME}" "${HOST}" "${PORT}"    
    fi
done

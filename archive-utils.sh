#!/bin/bash

# The script offers some utility functions for archiving the sports data.

################################
# Redirect the message to stderr
# Arguments:
#   None
#################################
error_echo (){
    >&2 echo "${@}"
}

################################
# Send a message to teams, and there are three level that you can choose.
# 1. Success 2. Warning 3. Error
# Arguments:
#   DB_NAME
#   COLLECTION_NAME
#   HOST
#   PORT
#   MESSAGE_LEVEL
#   MESSAGE[Optional]
#
# Reference:
#  - Microsoft actionable Messages
#   https://docs.microsoft.com/en-us/outlook/actionable-messages/message-card-reference#image-object
#################################
send_alert_message() {
    local DB_NAME=${1}
    local COLLECTION_NAME=${2}
    local HOST=${3}
    local PORT=${4}
    local MESSAGE_LEVEL=${5}
    local MESSAGE=${6}

    local STATUS
    local EXECUTION_TIME=$( date +"%Y-%m-%d %H:%M:%S" )

    MESSAGE=${MESSAGE:=""}
    if [ ! -z "${MESSAGE}" ];then
        MESSAGE=",{\"name\":\"Message\", \"value\": \"${MESSAGE}\"}"
    fi

    case ${MESSAGE_LEVEL} in
        "success")
            STATUS="<span style='color:green;font-weight:bold'>Success</span>"
        ;;
        "warning")
            STATUS="<span style='color:#aa42f5;font-weight:bold'>Warning</span>"
        ;;
        "error")
            STATUS="<span style='color:red;font-weight:bold'>Error</span>"
        ;;
    esac

    # This webhook url is for channel `archive-monitor` in group `monitor` 
    local WEBHOOK_URL="https://z53956242.webhook.office.com/webhookb2/97f50ea2-3154-44d3-b718-0eeac44feeee@a28f9002-914f-44d9-ad34-570123027b67"
    WEBHOOK_URL+="/IncomingWebhook/c545c16ff93f4f84bae5a886ba95ec0e/20139038-7401-4da9-8366-1c4373057d2f"
    
    local FORM_DATA="{
            \"@type\": \"MessageCard\", 
            \"@context\": \"http://schema.org/extensions\",
            \"summary\": \"Monitor archiving sports data task\",
            \"themeColor\": \"0076D7\",
            \"sections\": [
                {
                    \"activityTitle\": \"Archive Sports Data Task\",
                    \"activitySubtitle\": \"Detail Information\",
                    \"facts\": [
                        {\"name\": \"DataBase\",\"value\": \"${DB_NAME}\"},
                        {\"name\": \"Collection\",\"value\": \"${COLLECTION_NAME}\"},
                        {\"name\": \"Host\",\"value\": \"${HOST}\"},
                        {\"name\": \"Port\",\"value\": \"${PORT}\"},
                        {\"name\": \"Execution Time\", \"value\": \"${EXECUTION_TIME}\"},
                        {\"name\": \"Status\", \"value\": \"${STATUS}\"}${MESSAGE}
                    ],
                    \"markdown\": true
                }
            ]
        }"

    curl -X POST -H "Content-Type: application/json"\
        -d "${FORM_DATA}" ${WEBHOOK_URL}
}

#######################################
# Load configuration from specific path
# Arguments:
#   CONFIG_FILE: The path of the configuration
#######################################
load_config() {
    local CONFIG_FILE=${1}
    local ECHO_STRING
    local DB_NAMES

    # Get all db_name
    DB_NAMES=$( cat "${CONFIG_FILE}" | jq 'keys[]' | sed  's/"//g' )

    for db_name in "${DB_NAMES}"
    do
        # Get all collections names under the specific database.
        COLLECTION_NAMES=$( jq ".${db_name}|keys[]" ${CONFIG_FILE} | sed 's/"//g' )
        for collection_name in ${COLLECTION_NAMES}
        do
            # Get all parameters
            DAY_MINUS=$( jq ".${db_name}.\"${collection_name}\".days_minus" "${CONFIG_FILE}" )
            TIME_INTERVAL=$( jq ".${db_name}.\"${collection_name}\".time_interval" "${CONFIG_FILE}" )
            THRESHOLD_DOCS=$( jq ".${db_name}.\"${collection_name}\".threshold_docs" "${CONFIG_FILE}" )
            ECHO_STRING=(
                "{\"db_name\":\"$db_name\",\"collection_name\":\"${collection_name}\","
                "\"day_minus\":\"${DAY_MINUS}\",\"time_interval\":\"${TIME_INTERVAL}\","
                "\"threshold_docs\":\"${TIME_INTERVAL}\"}"
            )
            ECHO_STRING=$( printf "%s" "${ECHO_STRING[@]}" )
            echo "${ECHO_STRING}"
        done
    done
}

#######################################
# Split the time into small time block
# Arguments:
#   MIN_TIME
#   MAX_TIME
#   TIME_INTERVAL
#######################################
split_time_block() {
    # All of the input data use second as unit.
    local MIN_TIME=${1}
    local MAX_TIME=${2}
    local time_interval=${3}

    # Below variables are dynamic and used for each chunk size filter.
    local lower_time=${MIN_TIME}
    local upper_time=0
    local total_block=0

    if [ ${MIN_TIME} -ge ${MAX_TIME} ];then
        error_echo "ValueError: max_time: ${MAX_TIME} must be large than min_time: ${MIN_TIME}"
        exit 1
    fi

    while (( ${lower_time} < ${MAX_TIME} ))
    do
        upper_time=$(( ${lower_time}+${time_interval} ))
        
        if [ ${upper_time} -ge ${MAX_TIME} ];then
            upper_time=${MAX_TIME}
        fi
        total_block=$(( ${total_block}+1 ))
        echo "{\"lower_time\":${lower_time},\"upper_time\":${upper_time},\"block_num\":${total_block}}"
        # In the next round, the upper_time will be the lower_time.
        lower_time=${upper_time}
    done
}

#######################################
# Export MongoDB data with json format
# Arguments:
#   DB_USER
#   PASSWORD
#   HOST
#   DB_NAME
#   COLLECTION_NAME
#   DAY_MINUS (days)
#   TIME_INTERVAL (seconds)
# Below arguments have their default values
#   THRESHOLD_DOCS
#   TIME_FLAG_FOLDER
#   EXPORT_FOLDER
#######################################
export_mongo_data() {
    # Input arguments
    local DB_USER=${1}
    local PASSWORD=${2}
    local HOST=${3}
    local PORT=${4}
    local DB_NAME=${5}
    local COLLECTION_NAME=${6}
    local DAY_MINUS=${7}

    # The variable is for setting the time interval of each block. The unit is `second`.
    # If it doesn't give a value, its default value is 3600 seconds( 1 hour ).
    local TIME_INTERVAL=${8}
    TIME_INTERVAL="${TIME_INTERVAL:=3600}"
    local DAY_SECONDS=86400

    # The threshold is used for determining to dump whole day or per hour.
    # And the default value is 3000
    local THRESHOLD_DOCS=${9}
    THRESHOLD_DOCS="${THRESHOLD_DOCS:=3000}"
    
    # Both of time_flag_folder and export folder have default value.
    local TIME_FLAG_FOLDER=${10}
    local EXPORT_FOLDER=${11}
    TIME_FLAG_FOLDER="${TIME_FLAG_FOLDER:="./export-time-flag"}"
    EXPORT_FOLDER="${EXPORT_FOLDER:="./export-data"}"

    # Define the dateformat that use in mongo query.
    DATE_FORMAT="%Y-%m-%dT%H:%M:%S"

    # local ASCENDING=1
    # local DESCENDING=-1
    local CURRENT_TIME=$( date +"${DATE_FORMAT}" )
    local DUMP_UPPER_TIME=$( date -d "-${DAY_MINUS} days ${CURRENT_TIME}" +"${DATE_FORMAT}" )
    local DUMP_LOWER_TIME # The value is from mongoDB.

    # local TIME_FLAG_FOLDER='./export-time-flag'
    local TIME_FLAG_FILE="$( date -d "-${DAY_MINUS} days ${CURRENT_TIME}" +"%Y-%m-%d" ).json"
    local SAVE_TIME_FLAG_DIR="${TIME_FLAG_FOLDER}/${DB_NAME}/${COLLECTION_NAME}"
    local TIME_FLAG_FILE_PATH="${SAVE_TIME_FLAG_DIR}/${TIME_FLAG_FILE}"

    # Create a folder if it doesn't exist
    if [ ! -d "${SAVE_TIME_FLAG_DIR}" ];then
        mkdir -p "${SAVE_TIME_FLAG_DIR}"
    fi

    local DUMP_UPPER_TIME_UNIX_FORMAT
    local DUMP_LOWER_TIME_UNIX_FORMAT

    # Below variables is dynamical change, and it follow the iteration of function `split_time_block`.
    local lower_time
    local upper_time
    local block_num

    # Below variables are for output file
    local OUTPUT_FILE_PREFIX
    local OUTPUT_FILE_SUFFIX
    local OUTPUT_FILE_PATH
    # local EXPORT_FOLDER="./export-data"
    local EXPORT_DIR="${EXPORT_FOLDER}/${DB_NAME}/${COLLECTION_NAME}"
    local EXPORT_FILE_EXTENSION='json'

    # Create a folder if it doesn't exist
    if [ ! -d "${EXPORT_DIR}" ];then
        mkdir -p "${EXPORT_DIR}"
    fi

    local ALERT_MESSAGE
    ALERT_MESSAGE="${ALERT_MESSAGE:=""}"

    # Define the export data status
    EXPORT_DATA_STATUS=0
    local SUCCESS_STATUS=0
    local ERROR_STATUS=1
    local WARNING_STATUS=2
    
    # Export the last time with json format.
    mongoexport --authenticationDatabase admin -u ${DB_USER} -p ${PASSWORD} \
        -h "${HOST}:${PORT}" -d ${DB_NAME} -c ${COLLECTION_NAME}\
        --query="{\"created_at\":{\"\$lt\":\"${DUMP_UPPER_TIME}\"}}"\
        --sort="{\"created_at\":1}" --fields="_id,created_at" --limit=1\
        --type=json > ${TIME_FLAG_FILE_PATH}

    if [ "${?}" -ne 0 ];then
        # Send error notification by teams API
        ALERT_MESSAGE=(
            "MongoExport occurs some unexpected errors. "
            "Please check that. Export Arguments => Database: ${DB_NAME}, "
            "collection_name: ${COLLECTION_NAME}, host: ${HOST}, port: ${PORT}, "
            "DUMP_UPPER_TIME: ${DUMP_UPPER_TIME}"
        )
        ALERT_MESSAGE=$( printf "%s" "${ALERT_MESSAGE[@]}" )
        send_alert_message ${DB_NAME} ${COLLECTION_NAME} ${HOST} ${PORT} "error" "${ALERT_MESSAGE}"
        EXPORT_DATA_STATUS="${ERROR_STATUS}"
        return "${EXPORT_DATA_STATUS}"
    fi

    # Check whether the file is empty or not.
    if [ ! -s "${TIME_FLAG_FILE_PATH}" ];then
        ALERT_MESSAGE=(
            "The output file is empty file. file_path: ${TIME_FLAG_FILE_PATH}. "
            "It means it doesn't have new data before ${DAY_MINUS} days. "
            "It needs to check crawler server with backend team. "
            "Export Time Flag Arguments => DUMP_UPPER_TIME: ${DUMP_UPPER_TIME} "
        )
        ALERT_MESSAGE=$( printf "%s" "${ALERT_MESSAGE[@]}" )
        send_alert_message ${DB_NAME} ${COLLECTION_NAME} ${HOST} ${PORT} "warning" "${ALERT_MESSAGE}"
        EXPORT_DATA_STATUS="${WARNING_STATUS}"
        return "${EXPORT_DATA_STATUS}"
    fi
    
    DUMP_LOWER_TIME=$( jq '.created_at' "${TIME_FLAG_FILE_PATH}" | sed 's/"//g' )

    # Convert format date to unix-timestamp
    DUMP_UPPER_TIME_UNIX_FORMAT=$( date -d "${DUMP_UPPER_TIME}" +"%s" )
    DUMP_LOWER_TIME_UNIX_FORMAT=$( date -d "${DUMP_LOWER_TIME}" +"%s" )

    # Convert date format as file name
    OUTPUT_FILE_PREFIX=$( date -d "${DUMP_LOWER_TIME}" +"%Y-%m-%d" )
    OUTPUT_FILE_SUFFIX=$( date -d "${DUMP_UPPER_TIME}" +"%Y-%m-%d" )
    OUTPUT_FILE_PATH="${EXPORT_DIR}/${OUTPUT_FILE_PREFIX}-${OUTPUT_FILE_SUFFIX}.${EXPORT_FILE_EXTENSION}"

    local CHECK_QUERY
    local COUNT_DOCS
    for check_time_info in $( split_time_block "${DUMP_LOWER_TIME_UNIX_FORMAT}" "${DUMP_UPPER_TIME_UNIX_FORMAT}" "${DAY_SECONDS}" )
    do
        lower_check_time_unix_format=$( echo ${check_time_info} | jq '.lower_time' | sed 's/"//g' )
        upper_check_time_unix_format=$( echo ${check_time_info} | jq '.upper_time' | sed 's/"//g' )
        check_block_num=$( echo ${check_time_info} | jq '.block_num' | sed 's/"//g' )

        lower_check_time=$( date -d "@${lower_check_time_unix_format}" +"${DATE_FORMAT}" )
        upper_check_time=$( date -d "@${upper_check_time_unix_format}" +"${DATE_FORMAT}" )

        echo "================================================================================================================"
        echo "lower_check_time: ${lower_check_time}, upper_check_time: ${upper_check_time}, check_block_num: ${check_block_num}"

        # MongoDB count documents query
        CHECK_QUERY=(
            "db.getSiblingDB(\"${DB_NAME}\").runCommand({count: \"${COLLECTION_NAME}\", "
            "query: {\"created_at\": {\$gt: \"${lower_check_time}\", \$lt: \"${upper_check_time}\"}}})"
        )
        CHECK_QUERY=$( printf "%s" "${CHECK_QUERY[@]}" )

        # Count documents to check whether it exceeds the threshold.
        # If it doesn't have any document, it will skip this iteration.
        # If it exceeds the threshold, it will export data per hour, or export whole day data. 
        COUNT_DOCS=$( mongo admin -u "${DB_USER}" --host "${HOST}"\
                    --port "${PORT}" -p "${PASSWORD}" --eval "${CHECK_QUERY}"
        )
        COUNT_DOCS=$( echo "${COUNT_DOCS}" | grep -oE "\"n\"\s\:\s[0-9]+" | grep -oE "[0-9]+" )
        echo "There are ${COUNT_DOCS} documents druing the ${lower_check_time} to ${upper_check_time}."
        
        if [ ${COUNT_DOCS} -le ${THRESHOLD_DOCS} ] && [ ${COUNT_DOCS} -ne 0 ];then
            # Dump whole day data.
            echo "Start to export whole day data....."
            echo "ExportInfo => db_name: ${DB_NAME}, collection_name: ${COLLECTION_NAME}, lower_time: ${lower_check_time}, upper_time: ${upper_check_time}"
            mongoexport --authenticationDatabase admin -u ${DB_USER} -p ${PASSWORD} \
                -h "${HOST}:${PORT}" -d ${DB_NAME} -c ${COLLECTION_NAME}\
                --query="{\"created_at\":{\"\$gt\":\"${lower_check_time}\", \"\$lt\":\"${upper_check_time}\"}}"\
                --type=json >> ${OUTPUT_FILE_PATH}

            if [ "${?}" -ne 0 ];then
                # Delete docs after exporting the docs...
                echo "Start deleting the documents....."
                DELETE_QUERY=(
                    "db.getSiblingDB(\"${DB_NAME}\").runCommand({"
                    "delete: \"${COLLECTION_NAME}\", "
                    "deletes: [{q: {\"created_at\": {\$gt: \"${lower_check_time}\", \$lt: \"${upper_check_time}\"}}, limit: 0}]"
                    "})"
                )
                DELETE_QUERY=$( printf "%s" "${DELETE_QUERY[@]}" )
                DELETE_NUM=$( mongo admin  -u "${DB_USER}" --host "${HOST}"\
                            --port "${PORT}" -p "${PASSWORD}" --eval "${DELETE_QUERY}"
                )
                DELETE_NUM=$( echo "${DELETE_NUM}" | grep -oE "\"n\"\s\:\s[0-9]+" | grep -oE "[0-9]+" )
                echo "Delete ${DELETE_NUM} docs during the ${lower_check_time} and ${upper_check_time}"
            else
                # Send error notification by teams API
                ALERT_MESSAGE=(
                    "MongoExport occurs some unexpected errors. "
                    "Reture Status: ${?}, "
                    "Export Arguments => Database: ${DB_NAME}, "
                    "collection_name: ${COLLECTION_NAME}, host: ${HOST}, port: ${PORT}, "
                    "DUMP_UPPER_TIME: ${DUMP_UPPER_TIME}, DUMP_LOWER_TIME: ${DUMP_LOWER_TIME}, "
                    "upper_time: ${upper_check_time}, lower_time: ${lower_check_time}, "
                    "export_time_range: day"
                )
                ALERT_MESSAGE=$( printf "%s" "${ALERT_MESSAGE[@]}" )
                send_alert_message ${DB_NAME} ${COLLECTION_NAME} ${HOST} ${PORT} "error" "${ALERT_MESSAGE}"
            fi

        elif [ ${COUNT_DOCS} -gt ${THRESHOLD_DOCS} ];then
            # Dump per hours.
            echo "Start to export data per hour....."
            for dump_time_info in $( split_time_block "${lower_check_time_unix_format}" "${upper_check_time_unix_format}" "${TIME_INTERVAL}" )
            do
                lower_time=$( echo ${dump_time_info} | jq '.lower_time' | sed 's/"//g' )
                upper_time=$( echo ${dump_time_info} | jq '.upper_time' | sed 's/"//g' )
                block_num=$( echo ${dump_time_info} | jq '.block_num' | sed 's/"//g' )
                
                # Convert format and use to filter created_at.
                lower_time=$( date -d "@${lower_time}" +"${DATE_FORMAT}" )
                upper_time=$( date -d "@${upper_time}" +"${DATE_FORMAT}" )

                echo "-------------------------------------------------------------------------------------------------"
                echo "Export data per hour from ${lower_check_time} to ${upper_check_time}...."
                echo "ExportDataInfo => db_name: ${DB_NAME}, collection_name: ${COLLECTION_NAME}, lower_time: ${lower_time}, upper_time: ${upper_time}, block_num: ${block_num}"

                mongoexport --authenticationDatabase admin -u ${DB_USER} -p ${PASSWORD} \
                    -h "${HOST}:${PORT}" -d ${DB_NAME} -c ${COLLECTION_NAME}\
                    --query="{\"created_at\":{\"\$gt\":\"${lower_time}\", \"\$lt\":\"${upper_time}\"}}"\
                    --type=json >> ${OUTPUT_FILE_PATH}

                if [ "${?}" -eq 0 ];then
                    # Delete docs after exporting the docs...
                    echo "Start deleting the documents....."
                    DELETE_QUERY=(
                        "db.getSiblingDB(\"${DB_NAME}\").runCommand({"
                        "delete: \"${COLLECTION_NAME}\", "
                        "deletes: [{q: {\"created_at\": {\$gt: \"${lower_time}\", \$lt: \"${upper_time}\"}}, limit: 0}]"
                        "})"
                    )
                    DELETE_QUERY=$( printf "%s" "${DELETE_QUERY[@]}" )
                    DELETE_NUM=$( mongo admin  -u "${DB_USER}" --host "${HOST}"\
                                --port "${PORT}" -p "${PASSWORD}" --eval "${DELETE_QUERY}"
                    )
                    DELETE_NUM=$( echo "${DELETE_NUM}" | grep -oE "\"n\"\s\:\s[0-9]+" | grep -oE "[0-9]+" )
                    echo "Delete ${DELETE_NUM} docs during the ${lower_time} and ${upper_time}"
                else
                    # Send error notification by teams API
                    ALERT_MESSAGE=(
                        "MongoExport occurs some unexpected errors. "
                        "Reture Status: ${?}, "
                        "Export Arguments => Database: ${DB_NAME}, "
                        "collection_name: ${COLLECTION_NAME}, host: ${HOST}, port: ${PORT}, "
                        "DUMP_UPPER_TIME: ${DUMP_UPPER_TIME}, DUMP_LOWER_TIME: ${DUMP_LOWER_TIME}, "
                        "upper_time: ${upper_time}, lower_time: ${lower_time}, "
                        "export_time_range: hour"
                    )
                    ALERT_MESSAGE=$( printf "%s" "${ALERT_MESSAGE[@]}" )
                    send_alert_message ${DB_NAME} ${COLLECTION_NAME} ${HOST} ${PORT} "error" "${ALERT_MESSAGE}" 
                fi
            done
        else
            # Skip this iteration.
            echo "Next Iteration..."
            continue
        fi
    done

    if [ ! -s "${OUTPUT_FILE_PATH}" ];then
        ALERT_MESSAGE=(
            "The output file is empty file. file_path: ${OUTPUT_FILE_PATH} "
            "Please check the mongoDB by dump time information. "
            "Export Arguments => CURRENT_TIME: ${CURRENT_TIME}, DUMP_UPPER_TIME: ${DUMP_UPPER_TIME}, DUMP_LOWER_TIME: ${DUMP_LOWER_TIME}"
        )
        ALERT_MESSAGE=$( printf "%s" "${ALERT_MESSAGE[@]}" )
        send_alert_message ${DB_NAME} ${COLLECTION_NAME} ${HOST} ${PORT} "warning" "${ALERT_MESSAGE}"
        EXPORT_DATA_STATUS="${WARNING_STATUS}"
        return "${EXPORT_DATA_STATUS}"  
    fi
    return "${EXPORT_DATA_STATUS}" 
}

## Below functions have been implemented so far, but those functions can improve whole export task. 

##################################################
# Retry function when the function execute errror.
# Arguments:
#   DB_USER
#   PASSWORD
#   HOST
#   DB_NAME
#   COLLECTION_NAME
#   DAY_MINUS
###################################################
retry_func() {
    #TODO: Need to test and modify the parameter
    # Note: The input function need to return the variable ${EXECUTE_RESULT}

    local FUNC=${1} 
    shift 1 # Shift the all arguments to the left (original $1 gets lost)
    local ARGS=("${@}")
    echo "All input arguments: ${@}"

    local MAXIMUM_EXECUTE_TIMES=3
    # To set the sleep time between each execution.
    local TIME_INTERVAL=1
    local EXECUTE_TIME=0
    # How much time does it cost
    local START_TIME=$( date +%s )
    local ELAPSED_TIME=0

    while true;
    do
        # Place the function that you want to execute.
        ${FUNC} ${ARGS}
        EXECUTE_TIME=$(( ${EXECUTE_TIME}+1 ))
        # Conditition for stopping the while loop.
        if [ "${EXECUTE_RESULT}" = true ];then
            # success
            ELAPSED_TIME=$(( $( date +%s ) - ${START_TIME} ))
            echo -e "execute_time: ${EXECUTE_TIME}, total elapsed time: ${ELAPSED_TIME}.\n"
            break
        fi

        if [ ${EXECUTE_TIME} -eq ${MAXIMUM_EXECUTE_TIMES} ];then
            ELAPSED_TIME=$(( $( date +%s ) - ${START_TIME} ))
            echo "execute_time: ${EXECUTE_TIME}, elapsed_time: ${ELAPSED_TIME}"
            break
        else
            sleep ${TIME_INTERVAL}
        fi
    done
}

##################################################
# Upload files to the S3
# Arguments:
#   -p PROFILE
#   -s UPLOAD_PATH
#   -d UPLOAD_DESTINATION
#   -e UPLOAD_FILE_EXT
###################################################
upload_data_to_s3() {
    # Declare variables
    local PROFILE=${1}
    local UPLOAD_PATH=${2}
    local UPLOAD_DESTINATION=${3}
    local UPLOAD_FILE_EXT=${4}
    local upload_file_names=()
    local ARGS_INFO=()
    # Assign variables
    # while getopts 'p:s:d:e:' arg
    # do
    #     case ${arg} in
    #         'p') 
    #             PROFILE="${OPTARG}";;
    #         's') 
    #             UPLOAD_PATH="${OPTARG}";;
    #         'd') 
    #             UPLOAD_DESTINATION="${OPTARG}";;
    #         'e')
    #             UPLOAD_FILE_EXT="${OPTARG}";;
    #     esac
    # done
    ARGS_INFO=(
        "Args => s3_profile: ${PROFILE}, upload_path: ${UPLOAD_PATH}, "
        "upload_destination: ${UPLOAD_DESTINATION}, upload_file_ext: ${UPLOAD_FILE_EXT}"
    )
    ARGS_INFO=$( printf "%s" "${ARGS_INFO[@]}" )
    echo "${ARGS_INFO}"

    if [ -z "${PROFILE}" ];then
        num_profile=$( aws configure list-profiles | wc -l )
        if [ ${num_profile} -ne 1 ];then
            echo "Please specific the profile name for AWS cli use."
            exit 1
        else
            PROFILE=$( aws configure list-profiles )
        fi
    fi

    if [ -z "${UPLOAD_FILE_EXT}" ];then
        printf "Upload whole folder...\n upload_folder: ${UPLOAD_PATH}"
        RESULT=$( aws s3 cp --profile "${PROFILE}" "${UPLOAD_PATH}/" "${UPLOAD_DESTINATION}/" --recursive )
        echo "${RESULT}"
    else
        upload_file_names=($( find "${UPLOAD_PATH}" -maxdepth 1 -name "*.${UPLOAD_FILE_EXT}" | awk -F '/' '{print $NF}' ))
        echo "Upload file to S3..."
        for file_name in "${upload_file_names[@]}"
        do
            echo "Upload file_name: ${file_name}"
            RESULT=$( aws s3 cp --profile "${PROFILE}" "${UPLOAD_PATH}/${file_name}" "${UPLOAD_DESTINATION}/${file_name}" )
            echo "${RESULT}"
        done
    fi
}

# delete_mongo_data() {

# }

compress_data() {
    local FOLDER_PATH=${1}
    local DELETE_AFTER_COMPRESS=${2}
    DELETE_AFTER_COMPRESS=${DELETE_AFTER_COMPRESS:=false}
    local COMPRESS_FILE_EXT=".tar.gz"
    local log_info

    UNZIP_FILES=($( ls "${FOLDER_PATH}" | grep -oE ".*\.json" ))

    local fn_without_ext 
    local source_file_name
    local compress_file_name
    for file_name in "${UNZIP_FILES[@]}"
    do
        # Only get file name without extension.
        fn_without_ext=$( echo "${file_name}" | sed "s/\.json//g" )
        source_file_name="${FOLDER_PATH}/${file_name}"
        compress_file_name="${FOLDER_PATH}/${fn_without_ext}${COMPRESS_FILE_EXT}"
        
        tar -czf "${compress_file_name}" "${source_file_name}"
        
        if [ "${?}" -eq 0 ];then
            log_info=(
                "Successfully comperss file. source_file: ${source_file_name}, "
                "compress_file: ${compress_file_name}" 
            )
            log_info=$( printf "%s\n" "${log_info[@]}" )
            echo "${log_info}"
            if [ "${DELETE_AFTER_COMPRESS}" = true ];then
                rm "${source_file_name}"
            fi
        fi
    done
}

check_upload_s3() {
    local PROFILE=${1}
    local UPLOAD_FOLDER_PATH=${2}
    local S3_URI=${3}
    # DB Information
    local DB_NAME=${4}
    local COLLECTION_NAME=${5}
    local HOST=${6}
    local PORT=${7}

    local checked_result=0
    local not_uploaded_files=()
    local _not_uploaded_files=""
    local ALERT_MESSAGE=""
    local CHECK_FILES=()
    local COUNT_FILES

    #! Note that the S3 bucket directory needs to add slash.
    if [ "${S3_URI: -1}" != "/" ];then
        S3_URI="${S3_URI}/"
    fi

    COUNT_FILES=$( find "${UPLOAD_FOLDER_PATH}" -maxdepth 1 -name "*.tar.gz" | wc -l )
    if [ ${COUNT_FILES} -eq 0 ];then
        echo "There aren't any tar.gz file in the ${UPLOAD_FOLDER_PATH}"
        return
    fi

    CHECK_FILES=($( find "${UPLOAD_FOLDER_PATH}" -maxdepth 1 -name "*.tar.gz" | awk -F '/' '{print $NF}' ))
    for file_name in "${CHECK_FILES[@]}"
    do
        echo "Check file name: ${file_name}..."
        checked_result=$( aws s3 ls --profile "${PROFILE}" "${S3_URI}" | grep "${file_name}" | wc -l )
        if [ ${checked_result} -eq 0 ];then
            # Appending array element
            not_uploaded_files[${#not_uploaded_files[@]}]="${file_name}"
        fi
    done
    if [ ${#not_uploaded_files[@]} -gt 0 ];then
        # Send alert message
        _not_uploaded_files=$( printf "%s, " "${not_uploaded_files[@]}" )
        ALERT_MESSAGE=(
            "UploadFileError: Below files unsuccessfully upload to S3 bucket."
            "Upload Info => aws_profile: ${PROFILE}, upload_folder_path: ${UPLOAD_FOLDER_PATH}"
            "S3_URI: ${S3_URI}, not_uploaded_files: [${_not_uploaded_files}]"
        )
        ALERT_MESSAGE=$( printf "%s\n" "${ALERT_MESSAGE[@]}" )
        send_alert_message ${DB_NAME} ${COLLECTION_NAME} ${HOST} ${PORT} "error" "${ALERT_MESSAGE}"
    else
        # Send successfully uploaded message
        send_alert_message ${DB_NAME} ${COLLECTION_NAME} ${HOST} ${PORT} "success"
    fi
}


if [ "${1}" != '--source-only' ];then
    echo "ImportError: Invalid args: ${@}"
    exit 1
fi

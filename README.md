## Archive Sports Data on MongoDB

### Project Structure

```ascii
archive-sports-data
├── archive-config.json
├── archive-sports-data.sh
├── archive-utils.sh
|── remote-config.cfg
```

- `archive-config.json`: It records which database and collections need to be archive, and its specific archive arguments.
- `archive-sports-data.sh`: The main script records the whole archive flow. 
- `archive-utils.sh`: It offers utility functions to `archive-sports-data.sh`.
- `remote-config.cfg`: It records the information that we need to connect to the remote MongoDB.
- `remote-config-sample.cfg`: The sample of remote-config.cfg.

### How to execute the project?

- The project place on dba jump server `/usr/local/bin/DB/archive-mongo-data`.

- Before executing the script, you need to install [***jq***](https://stedolan.github.io/jq/) which is command-line JSON processor. 
  
- ***You have to create a config file that name is `remote-config.cfg`, and ensure the all files are in the same level. When you create the `remote-config.cfg`, you can refer to the format of `remote-config-sample.cfg`.***

  ```bash
  bash archive-sports-data.sh
  ```

- After archiving the data, it will automatically generate the two floders  `export-data` and `export-time-flag`. Both of folders also contain the sub-folders, and the name of sub-folders is named by `DB_NAME` and `COLLECTION_NAME`.
